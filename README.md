## Aerostack

Please visit the [Aerostack Wiki](https://github.com/aerostack/install/wiki) for a complete documentation.


Requirements: Linux Ubuntu 20.04 with ROS Noetic.

Installation method (specific application)

This installation method is appropriate when a subset of Aerostack components are necessary for a particular application. The installation method only downloads the code that is needed for the application.

Some examples of Aerostack applications can be found in the following link:

Example applications

Change directory where the application is going to be downloaded (for example, the directory $HOME/Desktop):

    $ cd $HOME/Desktop

Download the installation files:

    $ git clone https://gitlab.com/DudePLS/aerostack-install

Download the code corresponding to an Aerostack application. This code should include a file with the extension .repos that contains the list of repositories used by the application using the format used by vcstool. For example, if this code is in the repository https://gitlab.com/DudePLS/aerostack-teleoperation_gazebo , write the following command:

    $ git clone https://gitlab.com/DudePLS/aerostack-teleoperation_gazebo

Change directory:

    $ cd install

Execute installation command. The first argument of this command is the name of the file with the extension .repos (with the absolute path):

    $ ./install_from_source.sh $HOME/Desktop/aerostack-teleoperation_gazebo/application.repos

This command installs the Aerostack workspace in a default directory (the default directory is $HOME). If you want to install the code in a different directory, you can write the installation command including the name of the directory (with the absolute path) as a second argument. For example, the following command installs the Aerostack workspace in the directory /opt:

    $ ./install_from_source.sh $HOME/Desktop/aerostack-teleoperation_gazebo/application.repos /opt
